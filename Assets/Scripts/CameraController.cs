﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour {
	private FurinController Furin_;

	// Use this for initialization
	void Start () {
		Furin_ = GameObject.Find ("Furin").GetComponent<FurinController> ();
	}
	
	// Update is called once per frame
	void Update () {
        Debug.Log("Update");
		if (Input.GetKey (KeyCode.Space)) {
			Debug.Log ("Kick Furin!");
			Furin_.Accel (new Vector3(0.0f, 0.0f, 30.0f));
		}
	}
}

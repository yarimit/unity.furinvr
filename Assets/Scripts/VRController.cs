﻿using UnityEngine;
using System.Collections;

public class VRController : MonoBehaviour {
    public GameObject FurinPrefab_;

	// Use this for initialization
	void Start () {
        var trackedController = gameObject.GetComponent<SteamVR_TrackedController>();
        trackedController.TriggerClicked += (s, e) => {
            Ray raycast = new Ray(transform.position, transform.forward);   // コントローラからの直線Ray

            RaycastHit hitInfo;
            var hit = Physics.Raycast(raycast, out hitInfo, 10);

            if (hit) {
                GameObject pointingObject = hitInfo.transform.gameObject;
                var pointingFurin = pointingObject.GetComponentInParent<FurinController>();
                if (pointingFurin != null) {
                    pointingFurin.Accel(transform.forward);
                }
            }
        };

        trackedController.PadClicked += (s, e) => {
            Vector3 pos = transform.position + (transform.forward * 2);
            Instantiate(FurinPrefab_, pos, Quaternion.identity);

        };
    }


    // Update is called once per frame
    void Update () {
	
	}
}

﻿using UnityEngine;
using System.Collections;

public class FurinController : MonoBehaviour {
	private GameObject BaseChain_;
	private GameObject Bell_;
	private float DefaultRotationX_;

	// Use this for initialization
	void Start () {
		BaseChain_ = transform.FindChild("Chain10").gameObject;
		Bell_ = transform.FindChild("Bell").gameObject;
		DefaultRotationX_ = Bell_.transform.localEulerAngles.x;

		Debug.Log ("DefaultRotationX : " + DefaultRotationX_);
	}
	
	// Update is called once per frame
	void Update () {
		var rx = Mathf.Abs (Bell_.transform.localEulerAngles.x - DefaultRotationX_);
		if (rx > 30) {
			var auds = Bell_.GetComponent<AudioSource>();
			auds.PlayOneShot (auds.clip);
			//Debug.Log ("Rotate : " + Bell_.transform.localEulerAngles.x);
		}
	}

	public void Accel(Vector3 vector) {
        // Y軸は無視してvectorの方向を向く
        var lp = new Vector3(BaseChain_.transform.position.x + vector.x,
                             BaseChain_.transform.position.y,
                             BaseChain_.transform.position.z + vector.z);

        BaseChain_.transform.LookAt(lp);

        // X軸に対して揺らす
		var rb = BaseChain_.GetComponent<Rigidbody> ();
		float unit = 0.5f;
		iTween.RotateTo(BaseChain_, iTween.Hash("x", -90, "time", unit));
		iTween.RotateTo(BaseChain_, iTween.Hash("x", 90, "time", unit, "delay", unit));
		iTween.RotateTo(BaseChain_, iTween.Hash("x", 0, "time", unit, "delay", unit * 2));
    }
}
